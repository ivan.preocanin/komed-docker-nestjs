import { Controller, Get, Param } from '@nestjs/common';
import { ShowsService } from './shows.service';

@Controller('shows')
export class ShowsController {
  constructor(private readonly showsService: ShowsService) {}

  @Get(':show')
  async getShowData(@Param('show') show: string) {
    const res = await this.showsService.find(show);
    return res.data;
  }
}
