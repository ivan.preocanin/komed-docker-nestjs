import { Injectable, HttpService } from '@nestjs/common';

@Injectable()
export class ShowsService {
  constructor(private readonly httpService: HttpService) {}

  public find(show: string) {
    return this.httpService.get(`https://api.tvmaze.com/search/shows?q=${show}`).toPromise();
  }
}
