# komed-docker-nestjs

Two Nest.js services running in their own Docker containers

S1 will call API endpoint on service S2, and return a random episode from the
TV show of your choice

Clone the repo, then run

`docker-compose up`

then go to

`http://localhost:3000/episodes/batman`

or

`http://localhost:3000/episodes/the%20x%20files`