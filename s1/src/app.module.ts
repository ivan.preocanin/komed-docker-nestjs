import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EpisodesController } from './episodes/episodes.controller';
import { EpisodesService } from './episodes/episodes.service';

@Module({
  imports: [HttpModule],
  controllers: [AppController, EpisodesController],
  providers: [AppService, EpisodesService],
})
export class AppModule {}
