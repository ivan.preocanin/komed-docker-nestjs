import { Controller, Get, Param } from '@nestjs/common';
import { EpisodesService } from './episodes.service';

@Controller('episodes')
export class EpisodesController {
  constructor(private readonly episodesService: EpisodesService) {}

  @Get(':show')
  async getRandomEpisodeFromShow(@Param('show') show: string) {
    return await this.episodesService.getRandomEpisodeFromShow(show);
  }
}
