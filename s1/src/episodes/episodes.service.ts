import { Injectable, HttpService } from '@nestjs/common';

@Injectable()
export class EpisodesService {
  constructor(private readonly httpService: HttpService) {}

  public async getRandomEpisodeFromShow(show: string) {
    const episodes = await this.httpService.get(`http://s2:4000/shows/${show}`).toPromise();
    return episodes.data[Math.floor(Math.random() * episodes.data.length)];
  }
}
